import * as homePage from "../pageObjects/homePage";

module.exports = function () {
    this.Given(/^I am on the landing page$/, homePage.gotoPage);
    this.Then(/^I can see logo in navigation bar$/, homePage.hasLogo);
};

