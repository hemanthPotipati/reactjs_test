export const gotoProductPage = (product) => {
    browser.url('/');
    //browser.url('/en/tsuk/category/clothing-427/tops-443')
    openCategoryTab();
    openClothingTab();
    clickOnProductTab(product);
    isOnProductPage(product);
};

export const openCategoryTab = () => {
    browser.isVisible(".BurgerButton-bar");
    browser.click(".BurgerButton-bar");
};

export const openClothingTab = () => {
    browser.isVisible("//*[@class='TopNavMenu-parentListBlock']//*[contains(text(),'Clothing')]");
    browser.click("//*[@class='TopNavMenu-parentListBlock']//*[contains(text(),'Clothing')]");
};

export const clickOnProductTab = (product) => {
    browser.isVisible("//*[@class='Categories-listItem Categories-listItem208524']");
    browser.click("//*[@class='Categories-listItem Categories-listItem208524']");
};

export const isOnProductPage = (product) => {
    browser.isVisible("//*[@class='PlpContainer-productListContainer']//*[contains(text(),'"+product+"')]");
};

export const clickProductListTab = () => {
    browser.isVisible("//*[@class='ProductViews Filters-column']//*[contains(text(),'product')]");
    browser.click("//*[@class='ProductViews Filters-column']//*[contains(text(),'product')]");
};

export const clickOnFilterTab = () => {
    browser.isVisible("//*[@class='Button Filters-refineButton']");
    browser.click("//*[@class='Button Filters-refineButton']");
};

export const numberOfFiltersApplied = (numberOfFilters) => {
    browser.isVisible("//*[@class='Button Filters-refineButton']//*[contains(text(),'"+numberOfFilters+"')]");
};

export const hasProductList = () => {
    browser.isVisible(".ProductList");
};

export const hasNoFilters = () => {
    //if(browser.isVisible("//*[@class='Button Filters-refineButton']//*[contains(text(),'(')]")){}
};