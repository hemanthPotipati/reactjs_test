Feature: Product listing -> filter by colour
  Background: As a customer
  I would like to refine my search results based on color
  So that I can view the products of my liking

  Scenario: Example scenario
    Given I am on the landing page
    Then I can see logo in navigation bar

    Scenario Outline: Customer filters by colour
      Given I am viewing product list "<productList>"
      And I filter the product list
      And I filter by option "<filterOption>"
      And I select colour "<colour>"
      When I apply these filters
      Then I should see filter button has "<filterNumber>" filter
      And I should see filter returns a product list

      Examples:
      |productList|filterOption|colour|filterNumber|
      |Tops       |Colour      |Black |1           |

  Scenario: Customer filters by multiple colours
    Given I am viewing product list "Tops"
    And I filter the product list
    And I filter by option "Colour"
    And I select colour "White"
    And I select colour "Black"
    When I apply these filters
    Then I should see filter button has "1" filter
    And I should see filter returns a product list

  Scenario: Customer clears colour filters
    Given I am viewing product list "Tops"
    And I filter the product list
    And I filter by option "Colour"
    And I select colour "White"
    And I clear all filters
    When I apply these filters
    Then I should see filter button has no filters
    And I should see filter returns a product list
